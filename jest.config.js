module.exports = {
  moduleNameMapper:{
    "\\.(css|less|sass|scss)$": "<rootDir>/__mocks__/styleMock.js",
  },
  preset: 'ts-jest',
  resetMocks: true,
  testEnvironment: 'jest-environment-jsdom-global',
  //TODO Remettre comme avant
  testMatch: ['**/__tests__/**/*.test.ts'],
  // transformIgnorePatterns: ['<rootDir>.*(node_modules)(?!.*@barba.*).*$'],
  verbose: true
};
