import { BlogService, PostService } from '@flexo/blogging.client'
import { RestService } from '@flexo/rest'
//import { AuthenticationService } from '@flexo/rest/axios-oauth2/authentication'
import { Auth } from './auth'
import { $elemsAuth, $elemsBlogs, $elemsPosts } from './elements'
import { print } from './print-method'

export function error(err: Error) {
  console.error(err)
}

export class TestService {
  protected restService: RestService
  protected blogService: BlogService
  protected postService: PostService
  constructor() {}
  /*
  private async authenticate(): Promise<string> {
    const authRest = new AuthenticationService(
      'https://demo-corpo.flexocms.com',
      error
    )
    var resultAuth = await authRest.getAuthorizationCode({
      url: 'https://demo-corpo.flexocms.com/connect/token',
      grant_type: 'password', //'client_credentials', //'authorization_code',
      client_id: 'SampleSDK', //'Flexo_App',
      client_secret: 'test-sdk', //'1q2w3e*',
      username: 'admin',
      password: '1q2w3E*',
    })
    return resultAuth.access_token
  }
*/
  async init(access_token: string) {
    //var access_token = await this.authenticate()
    this.restService = new RestService({
      instanceParams: {
        baseURL: 'https://demo-corpo.flexocms.com',
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      },
    })

    this.blogService = new BlogService(this.restService)
    this.postService = new PostService(this.restService)
  }

  /**
   * Blog Service
   */

  async getBlogList() {
    const list = await this.blogService.getList({
      maxResultCount: 20,
    })
    print(list)
  }

  async getBlogById(blogId: string) {
    const blogPost = await this.blogService.get(blogId)
    print(blogPost)
  }

  async createBlog() {
    const newBlog = await this.blogService.create({
      name: 'my name',
      slug: 'my slug',
      description: 'my desc',
    })

    print(newBlog)
  }

  async updateBlog(blogId: string) {
    const updateBlog = await this.blogService.update(blogId, {
      name: 'my new name',
      slug: 'my slug',
      description: 'my desc',
    })

    print(updateBlog)
  }

  async deleteBlogById(blogIdToDelete: string) {
    const deletedBlog = await this.blogService.delete(blogIdToDelete)
    print(deletedBlog)
  }

  async getBlogBySlug(slug: string) {
    const blogPost = await this.blogService.getBySlug(slug)
    print(blogPost)
  }

  /**
   * Post Service
   */
  async getPostList() {
    const list = await this.postService.getList({
      maxResultCount: 20,
    })
    print(list)
  }

  async getPostById(postId: string) {
    const returnPost = await this.postService.get(postId)
    print(returnPost)
  }

  async createPost() {
    const newPost = await this.postService.create({
      slug: 'my-slug',
      blogId: 'blogId',
      title: 'my title',
    })

    print(newPost)
  }

  async updatePost(postId: string) {
    const updatedPost = await this.postService.update(postId, {
      slug: 'my-slug',
      blogId: 'blogId',
      title: 'my title',
    })

    print(updatedPost)
  }

  async deletePostById(postIdToDelete: string) {
    const deletedPost = await this.postService.delete(postIdToDelete)
    print(deletedPost)
  }

  async getPostBySlug(blogSlug: string, slug: string) {
    const returnPost = await this.postService.getBySlug(blogSlug, slug)
    print(returnPost)
  }
}

export function observeAuth() {
  $elemsAuth.authLogin.addEventListener('click', _ => {
    auth.login()
  })

  $elemsAuth.authLogout.addEventListener('click', _ => {
    auth.logout()
  })
}

export function observeBlog() {
  $elemsBlogs.getList.addEventListener('click', _ => {
    testService.getBlogList()
  })
  $elemsBlogs.getById.addEventListener('click', _ => {
    testService.getBlogById('2')
  })
  $elemsBlogs.create.addEventListener('click', _ => {
    testService.createBlog()
  })
  $elemsBlogs.update.addEventListener('click', _ => {
    testService.updateBlog('2')
  })
  $elemsBlogs.delete.addEventListener('click', _ => {
    testService.deleteBlogById('2')
  })
}

export function observePosts() {
  $elemsPosts.getList.addEventListener('click', _ => {
    testService.getPostList()
  })
  $elemsPosts.getById.addEventListener('click', _ => {
    testService.getPostById('2')
  })
  $elemsPosts.create.addEventListener('click', _ => {
    testService.createPost()
  })
  $elemsPosts.update.addEventListener('click', _ => {
    testService.updatePost('2')
  })
  $elemsPosts.delete.addEventListener('click', _ => {
    testService.deletePostById('2')
  })
}

observeAuth()

var testService = new TestService()
var auth = Auth.getInstance()
auth.process().then(response => {
  if (response !== null && response.access_token !== null) {
    testService.init(response.access_token).then(_ => {
      observeBlog()
      observePosts()
    })
  }
})
