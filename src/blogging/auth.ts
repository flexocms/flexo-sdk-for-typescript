import OIDC, { UserManagerSettings } from 'oidc-client'

export class Auth {
  private constructor() {
    OIDC.Log.logger = console
    OIDC.Log.level = OIDC.Log.DEBUG
    this.userMan = new OIDC.UserManager(Auth.OidcSettings)
  }

  private static OidcSettings: UserManagerSettings = {
    automaticSilentRenew: true,
    authority: 'https://demo-corpo.flexocms.com',
    client_id: 'SampleSDK',
    client_secret: 'test-sdk',
    redirect_uri: window.location.protocol + '//' + window.location.host,
    post_logout_redirect_uri:
      window.location.protocol + '//' + window.location.host,
    silent_redirect_uri: window.location.protocol + '//' + window.location.host,
    response_type: 'code',
    scope: 'openid profile Flexo',
    loadUserInfo: true,
    userStore: new OIDC.WebStorageStateStore({ store: window.localStorage }),
  }

  private static instance: Auth
  private static readonly KEY_FOLLOW = 'my_app_follow_action_required'
  private static readonly KEY_PROCESS_LOGIN = 'login'
  private static readonly KEY_PROCESS_LOGOUT = 'logout'

  private userMan: OIDC.UserManager
  private user: OIDC.User

  public static getInstance(): Auth {
    if (Auth.instance === undefined) {
      Auth.instance = new Auth()
    }
    return Auth.instance
  }

  public process(): Promise<OIDC.User | null> {
    if (this.mustFollowAuthLogin()) {
      return this.processSigninResponse().then(response => {
        return response
      })
    } else if (this.mustFollowAuthLogout()) {
      return this.processSignoutResponse().then(response => {
        return null
      })
    } else {
      return this.getUser()
    }
  }

  public getUser(): Promise<OIDC.User | null> {
    return this.userMan.getUser()
  }

  public login(): void {
    this.userMan.createSigninRequest().then(response => {
      this.getStorage().setItem(Auth.KEY_FOLLOW, Auth.KEY_PROCESS_LOGIN)
      window.location.href = response.url
    })
  }

  public logout(): void {
    this.userMan.removeUser()
    this.userMan.createSignoutRequest().then(signout_request => {
      window.location.href = signout_request.url
    })
  }

  private processSigninResponse(): Promise<OIDC.User> {
    this.getStorage().removeItem(Auth.KEY_FOLLOW)
    return this.userMan.signinRedirectCallback()
  }

  private processSignoutResponse(): Promise<void> {
    this.getStorage().removeItem(Auth.KEY_FOLLOW)
    return this.userMan.processSignoutResponse().then(response => {
      return this.userMan.removeUser()
    })
  }

  private mustFollowAuthLogin(): boolean {
    return (
      window.localStorage.getItem(Auth.KEY_FOLLOW) === Auth.KEY_PROCESS_LOGIN
    )
  }

  private mustFollowAuthLogout(): boolean {
    return (
      window.localStorage.getItem(Auth.KEY_FOLLOW) === Auth.KEY_PROCESS_LOGOUT
    )
  }

  private getStorage(): Storage {
    return localStorage
  }
}
