export const $elemsAuth = {
  authLogin: document.querySelector('.js-auth-login'),
  authLogout: document.querySelector('.js-auth-logout'),
}
export const $elemsBlogs = {
  getList: document.querySelector('.js-blog-get-list'),
  getById: document.querySelector('.js-blog-get-by-id'),
  create: document.querySelector('.js-blog-create'),
  update: document.querySelector('.js-blog-update'),
  delete: document.querySelector('.js-blog-delete'),
}

export const $elemsPosts = {
  getList: document.querySelector('.js-post-get-list'),
  getById: document.querySelector('.js-post-get-by-id'),
  create: document.querySelector('.js-post-create'),
  update: document.querySelector('.js-post-update'),
  delete: document.querySelector('.js-post-delete'),
}
